# 有爱编辑器 

这个readerme.md文档是基于当前的版本1.7.1的写的,1.7.x以下的可以查看之前写的帖子
#### ver 1.0 测试版 [lua编写说明](http://uiwow.com/forum.php?mod=redirect&goto=findpost&ptid=8770&pid=233262)

#### ver 1.6 [相关说明](http://uiwow.com/thread-9232-1-1.html)

-------
这是一个使用lua作为配置文件的mysql数据库可视化编辑器.

例如编辑表`_editors`:
![_editors表](https://images.gitee.com/uploads/images/2020/1126/220929_15d6d632_72264.png "屏幕截图.png")
新建lua文件,写入如下内容即可使用.
```
SetEditor("uieditors","_editors",{
    Name = "有爱编辑器表格设计器",
    Fields = {
        {field = "ID", tooltip = "编辑器id,用于标识编辑器,唯一,可与_editors_field表对应", name = "编辑器ID",isFilter = true, },
        {field = "Database", tooltip = "编辑器的所在数据库", name = "数据库名",isFilter = true, },
        {field = "Table", tooltip = "需要编辑的表格名", name = "表格名",isFilter = true, },
        {field = "Name", tooltip = "编辑器的名称", name = "编辑器名称",isFilter = true, },
        {field = "MysqlHost", tooltip = "数据库的地址或ip\n如果表格和setting.lua中所设置的数据库不是同一个的话,请设置这里的相关参数连接外部数据库",isFilter = true, },
        {field = "MysqlPort", tooltip = "数据库端口\n如果表格和setting.lua中所设置的数据库不是同一个的话,请设置这里的相关参数连接外部数据库",isFilter = true, },
        {field = "MysqlUser", tooltip = "数据库用户名\n如果表格和setting.lua中所设置的数据库不是同一个的话,请设置这里的相关参数连接外部数据库",isFilter = true, },
        {field = "MysqlPassword", tooltip = "数据库密码\n如果表格和setting.lua中所设置的数据库不是同一个的话,请设置这里的相关参数连接外部数据库",},
    }
})
```
![预览](https://images.gitee.com/uploads/images/2020/1126/220741_b98d8aea_72264.gif "a1.gif")